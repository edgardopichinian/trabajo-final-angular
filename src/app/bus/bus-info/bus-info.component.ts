import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Brand} from "../../domain/brand";
import {Model} from "../../domain/model";
import {ActivatedRoute, Router} from "@angular/router";
import {BusService} from "../bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Bus} from "../../domain/bus";

@Component({
  selector: 'bus-info',
  templateUrl: './bus-info.component.html',
  styleUrls: ['./bus-info.component.css']
})
export class BusInfoComponent implements OnInit {

  formBus: FormGroup = this.fb.group({
    id: [null, []],
    licensePlate: ['', [Validators.required, Validators.pattern(/^[A-Z]{3}\d{3}$/)]],
    numberOfSeats: ['', [Validators.required, Validators.min(5), Validators.max(20)]],
    model: [null, [Validators.required]],
  })

  loading: boolean = false;
  brands: Brand[] = [];
  models: Model[] = [];

  brandSelected: number = 0;

  constructor(private router: ActivatedRoute,
              private fb: FormBuilder,
              private busService: BusService,
              private route: Router,
              private snackBar: MatSnackBar) {

  }

  ngOnInit(): void {

    this.busService.findAllBrands().subscribe(b =>{
      this.brands = b;
    })

    this.router.paramMap.subscribe(param => {
      let paramId = param.get("id");
      this.loading = true;
      if (paramId != null) {
        const id = parseInt(paramId);
        this.busService.findOne(id).subscribe(bus => {
          this.buildForm(bus);
          this.loading = false;
        }, error => {
          this.loading = false;
          this.snackBar.open(error, "", {duration: 2000});
          this.goToBack();
        });
      } else {
        this.buildForm(null);
        this.loading = false;
      }
    })
  }

  get fc(){
    return this.formBus.controls;
  }

  goToBack(){
    this.route.navigate(['buses', 'list']);
  }

  buildForm(bus: Bus | null) {
    if(bus != null){
      this.formBus.patchValue({
        id: bus.id,
        licensePlate: bus.licensePlate,
        numberOfSeats: bus.numberOfSeats,
        model: bus.model.id});

      this.brandSelected = bus.model.brand.id;
      this.onSelect(this.brandSelected);
    }
  }

  save(){
    let indexBrand = this.brands.findIndex(obj => obj.id === this.brandSelected);
    let brand: Brand = this.brands[indexBrand];

    let indexModel = this.models.findIndex(obj => obj.id === this.formBus.get(["model"])?.value);
    let model: Model = this.models[indexModel];

    model.brand = brand;

    const bus = new Bus(
      this.formBus.get(["id"])?.value,
      this.formBus.get(["licensePlate"])?.value,
      this.formBus.get(["numberOfSeats"])?.value,
      model);

    this.loading = true;
    if(bus.id == null) {
      //CREATE
      this.busService.create(bus).subscribe(b => {
        this.snackBar.open("El colectivo se creó con éxito", "Exito", {duration: 2000});
        this.goToBack();
      }, error => {
        this.loading = false;
        this.snackBar.open(error, "Error", {duration: 2000});
      })
    } else {
      //UPDATE
      this.busService.update(bus).subscribe(b => {
        this.snackBar.open("El colectivo se actualizó con éxito", "Exito", {duration: 2000});
        this.goToBack();
      }, error => {
        this.loading = false;
        this.snackBar.open(error, "Error", {duration: 2000});
      })
    }
  }

  onSelect(brand_id: number){
    this.busService.findAllModels(brand_id).subscribe(m => {
      this.models = m;
    });
  }
}
