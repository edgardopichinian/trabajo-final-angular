import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusRoutingModule } from './bus-routing.module';
import { BusesListComponent } from './buses-list/buses-list.component';
import { BusInfoComponent } from './bus-info/bus-info.component';
import {BusService} from "./bus.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../material/material.module";
import {DialogModule} from "../../dialog/dialog.module";

@NgModule({
  declarations: [
    BusesListComponent,
    BusInfoComponent
  ],
  imports: [
    CommonModule,
    BusRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
    DialogModule
  ],
  providers: [BusService]
})
export class BusModule { }
