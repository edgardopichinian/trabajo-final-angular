import {Component, OnInit, ViewChild} from '@angular/core';
import {Bus} from "../../domain/bus";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BusService} from "../bus.service";
import {DialogData} from "../../../dialog/dialog-data";
import {DeleteConfirmDialogComponent} from "../../../dialog/delete-confirm-dialog/delete-confirm-dialog.component";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'buses-list',
  templateUrl: './buses-list.component.html',
  styleUrls: ['./buses-list.component.css']
})
export class BusesListComponent implements OnInit {

  displayedColumns: String[] = ['id', 'licensePlate', 'numberOfSeats',
    'brandName', 'modelName', 'option'];

  buses = new MatTableDataSource<Bus>();
  loading: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;

  constructor(private router: Router,
              private busService: BusService,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.findAll();
  }

  findAll() {
    this.loading = true;
    this.busService.findAll().subscribe(list => {
      this.buses = new MatTableDataSource<Bus>(list);
      this.buses.paginator = this.paginator
      this.loading = false;
    });
  }

  goToDetail(bus: Bus | null) {
    if (bus == null)
      this.router.navigate(['buses', 'info']);
    else
      this.router.navigate(['buses', 'info', {id:bus.id}]);
  }

  delete(id: number) {
    let dialogData = new DialogData(null,
      "¿Está seguro que desea eliminar el colectivo?", "Confirmación para eliminar");
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: '300px',
      height: 'auto',
      minHeight: 200,
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'delete'){
        this.loading = true;
        this.busService.delete(id).subscribe(b => {
            this.findAll();
            this.snackBar.open("El colectivo se eliminó con éxito", 'Éxito', {duration: 2000});
          },
          error => {
            this.snackBar.open(error, "Error", {duration: 2000});
            this.loading = false;
          });
      }
    });
  }
}
