import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BusesListComponent} from "./buses-list/buses-list.component";
import {BusInfoComponent} from "./bus-info/bus-info.component";

const routes: Routes = [
  {path: 'list', component: BusesListComponent},
  {path: 'info', component: BusInfoComponent},
  {path: '', redirectTo: 'list', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusRoutingModule { }
