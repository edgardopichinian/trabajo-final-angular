import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Bus} from "../domain/bus";
import {catchError, map} from "rxjs/operators";
import {Brand} from "../domain/brand";
import {Model} from "../domain/model";

@Injectable({
  providedIn: 'root'
})
export class BusService {

  private resourceUrl: string = environment.backUrl + "buses";
  private resourceUrlBrand: string = environment.backUrl + "brand";
  private resourceUrlModel: string = environment.backUrl + "model";

  constructor(private http: HttpClient) {

  }

  public findAll(): Observable<Bus[]> {
    return this.http.get<Bus[]>(this.resourceUrl).pipe(
      map(buses =>
        buses.map(b => new Bus(b.id, b.licensePlate, b.numberOfSeats,
          b.model)
        )));
  }

  public findOne(id: number):  Observable<Bus | null> {
    return this.http.get<Bus>(this.resourceUrl + "/" + id).pipe(
      catchError( err => {
        return throwError("El colectivo no existe.");
      }),
      map(b => new Bus(b.id, b.licensePlate, b.numberOfSeats,
        b.model)
      ));
  }

  public create(bus: Bus): Observable<any> {
    return this.http.post<any>(this.resourceUrl, bus).pipe(
      catchError(err => {
        console.log("Error");
        return throwError("El colectivo no puede ser creado.");
      }));
  }

  public update(bus: Bus): Observable<any> {
    return this.http.put<any>(this.resourceUrl, bus).pipe(
      catchError(err => {
        return throwError("El colectivo no pudo ser actualizado.");
      }));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.resourceUrl + "/" + id).pipe(
      catchError(err => {
        return throwError("El colectivo no pudo ser eliminado.");
      }));
  }

  public findAllBrands(): Observable<Brand[]>{
    return this.http.get<Brand[]>(this.resourceUrlBrand).pipe(
      map(marcas =>
        marcas.map(m => new Brand(m.id, m.name, m.models)
        )));
  }

  public findAllModels(brand_id: number): Observable<Model[]> {
    return this.http.get<Model[]>(this.resourceUrlModel + "/" + brand_id).pipe(
      map(modelos =>
        modelos.map(m => new Model(m.id, m.name, m.brand)
        )));
  }
}
