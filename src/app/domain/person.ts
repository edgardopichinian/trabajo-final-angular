export class Person {
  id: number;
  firstName: string;
  lastName: string;
  age: number;

  constructor(id: number, firsName: string, lastName: string, age: number) {
    this.id = id;
    this.firstName = firsName;
    this.lastName = lastName;
    this.age = age;
  }

  public showName() : string {
    return this.firstName + ", " + this.lastName;
  }

}
