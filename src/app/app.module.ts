import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialModule} from "./material/material.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { LayoutComponent } from './layout/layout.component';
import { UserShowPipe } from './pipes/user-show.pipe';
import {AuthenticationService} from "./security/service/authentication.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "./security/interceptor/token.interceptor";
import {RouterModule} from "@angular/router";
import {BusModule} from "./bus/bus.module";

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    LoginFormComponent,
    LayoutComponent,
    UserShowPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    BusModule
  ],
  providers: [AuthenticationService,
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
