import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PersonsListComponent} from "../person/persons-list/persons-list.component";
import {PersonInfoComponent} from "../person/person-info/person-info.component";
import {TripsListComponent} from "./trips-list/trips-list.component";
import {TripInfoComponent} from "./trip-info/trip-info.component";

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {
    path: 'list',
    component: TripsListComponent
  },
  {
    path: 'info',
    component: TripInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripRoutingModule { }
