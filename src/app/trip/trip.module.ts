import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TripRoutingModule } from './trip-routing.module';
import { TripInfoComponent } from './trip-info/trip-info.component';
import { TripsListComponent } from './trips-list/trips-list.component';
import {TripService} from "./trip.service";
import {MaterialModule} from "../material/material.module";
import { BusDetailDialogComponent } from './bus-detail-dialog/bus-detail-dialog.component';


@NgModule({
  declarations: [
    TripInfoComponent,
    TripsListComponent,
    BusDetailDialogComponent
  ],
    imports: [
        CommonModule,
        TripRoutingModule,
        MaterialModule
    ],
  providers: [
    TripService
  ]
})
export class TripModule { }
