import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {Trip} from "../domain/trip";
import {catchError, map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TripService {

  private resourceUrl: string = environment.backUrl + "trips";

  constructor(private http: HttpClient ) { }

  public findAll(): Observable<Trip[]> {
    return this.http.get<Trip[]>(this.resourceUrl).pipe(
      map(trips =>
        trips.map(t => new Trip(t.id, t.departure, t.destination, t.bus, t.passengers, t.startDate, t.endDate)
        ))
    );
  }

  public findOne(id: number):  Observable<Trip | null> {
    return this.http.get<Trip>(this.resourceUrl + "/" + id).pipe(
      catchError( err => {
        return throwError("El viaje no existe.");
      }),
      map(t => new Trip(t.id, t.departure, t.destination, t.bus, t.passengers,
        t.startDate, t.endDate)
      ));
  }

  public create(trip: Trip): Observable<any> {
    return this.http.post<any>(this.resourceUrl, trip).pipe(
      catchError(err => {
        console.log("Error");
        return throwError("El viaje no puede ser creado.");
      }));
  }

  public update(trip: Trip): Observable<any> {
    return this.http.put<any>(this.resourceUrl, trip).pipe(
      catchError(err => {
        return throwError("El viaje no pudo ser actualizado.");
      }));
  }

}
