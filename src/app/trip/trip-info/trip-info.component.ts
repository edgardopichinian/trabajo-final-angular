import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {BusService} from "../../bus/bus.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TripService} from "../trip.service";
import {Person} from "../../domain/person";
import {Bus} from "../../domain/bus";
import {Trip} from "../../domain/trip";
import {DateAdapter} from "@angular/material/core";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {PersonService} from "../../person/person.service";

@Component({
  selector: 'trip-info',
  templateUrl: './trip-info.component.html',
  styleUrls: ['./trip-info.component.css']
})
export class TripInfoComponent implements OnInit {

  formTrip: FormGroup = this.fb.group({
    id: [null, []],
    departure: ['', [Validators.required]],
    destination: ['', [Validators.required]],
    bus: ['', [Validators.required]],
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]],
    passengers: [null],
  })

  loading: boolean = false;
  buses: Bus[] = [];
  cantNumberOfSeats: number = 0;

  /*table*/
  displayedColumns: string[] = ['select', 'id', 'firstName', 'lastName'];
  dataSource = new MatTableDataSource<Person>();
  selection = new SelectionModel<Person>(true, []);
  persons: Person[] = [];


  constructor(private router: ActivatedRoute,
              private fb: FormBuilder,
              private tripService: TripService,
              private busService: BusService,
              private personService: PersonService,
              private route: Router,
              private snackBar: MatSnackBar,
              private dateAdapter: DateAdapter<Date>) {
    this.dateAdapter.setLocale('ES');
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Person): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row}`;
  }

  ngOnInit(): void {

    this.findAllPersons();

    this.findAllBuses();

    this.router.paramMap.subscribe(param => {
      let paramId = param.get("id");
      this.loading = true;
      if (paramId != null) {
        const id = parseInt(paramId);
        this.tripService.findOne(id).subscribe(trip => {
          this.buildForm(trip);
          this.loading = false;
        }, error => {
          this.loading = false;
          this.snackBar.open(error, "", {duration: 2000});
          this.goToBack();
        });
      } else {
        this.buildForm(null);
        this.loading = false;
      }
    })
  }

  findAllPersons() {
    this.loading = true;
    this.personService.findAll().subscribe((list: Person[]) => {
      this.persons = list;
      this.dataSource.data = this.persons;
      this.loading = false;
    });
  }

  findAllBuses() {
    this.loading = true;
    this.busService.findAll().subscribe(b =>{
      this.buses = b;
      this.loading = false;
    });
  }

  goToBack(){
    this.route.navigate(['trips', 'list']);
  }

  buildForm(trip: Trip | null) {
    if(trip != null){
      this.formTrip.patchValue({
        id: trip.id,
        departure: trip.departure,
        destination: trip.destination,
        bus: trip.bus.id,
        startDate: new Date(trip.startDate * 1000),
        endDate: new Date(trip.endDate * 1000),
        passengers: trip.passengers
      });

      this.passengerSelection(trip.passengers);
      this.cantNumberOfSeats = trip.bus.numberOfSeats - this.selection.selected.length;
    }
  }

  private passengerSelection(passengers: Person[]){
    passengers.forEach( (person: Person) => {
      let indexPerson = this.persons.findIndex(obj => obj.id === person.id);
      this.selection.toggle(this.dataSource.data[indexPerson]);
    });
  }

  get fc(){
    return this.formTrip.controls;
  }

  save(){

    let indexBus = this.buses.findIndex(obj => obj.id === this.formTrip.get(["bus"])?.value);
    let bus: Bus = this.buses[indexBus];

    if (this.selection.selected.length > bus.numberOfSeats) {
      this.snackBar.open("La cantidad de pasajeros debe ser menor o igual a "+ bus.numberOfSeats, "Error", {duration: 3000});
      return;
    }

    if (this.selection.selected.length == 0) {
      this.snackBar.open("Debe seleccionar al menos 1 pasajero", "Error", {duration: 3000});
      return;
    }

    const trip = new Trip(
      this.formTrip.get(["id"])?.value,
      this.formTrip.get(["departure"])?.value,
      this.formTrip.get(["destination"])?.value,
      bus,
      this.selection.selected,
      this.formTrip.get(["startDate"])?.value / 1000,
      this.formTrip.get(["endDate"])?.value / 1000
    );

    this.loading = true;
    if(trip.id == null) {
      //CREATE
      this.tripService.create(trip).subscribe(t => {
        this.snackBar.open("El viaje se creó con éxito", "Exito", {duration: 2000});
        this.goToBack();
      }, error => {
        this.loading = false;
        this.snackBar.open(error, "Error", {duration: 2000});
      })
    } else {
      //UPDATE
      this.tripService.update(trip).subscribe(t => {
        this.snackBar.open("El viaje se actualizó con éxito", "Exito", {duration: 2000});
        this.goToBack();
      }, error => {
        this.loading = false;
        this.snackBar.open(error, "Error", {duration: 2000});
      })
    }
  }

  comparisonEnddateValidator(): any {
    let ldStartDate = this.formTrip.value['startDate'];
    let ldEndDate = this.formTrip.value['endDate'];

    let startnew = new Date(ldStartDate);
    let endnew = new Date(ldEndDate);
    if (startnew > endnew) {
      return this.formTrip.controls['endDate'].setErrors({ 'invaliddaterange': true });
    }

    let oldvalue = startnew;
    this.formTrip.controls['startDate'].reset();
    this.formTrip.controls['startDate'].patchValue(oldvalue);
    return this.formTrip.controls['startDate'].setErrors({ 'invaliddaterange': false });
  }

  comparisonStartdateValidator(): any {
    let ldStartDate = this.formTrip.value['startDate'];
    let ldEndDate = this.formTrip.value['endDate'];

    let startnew = new Date(ldStartDate);
    let endnew = new Date(ldEndDate);
    if (startnew > endnew) {
      return this.formTrip.controls['startDate'].setErrors({ 'invaliddaterange': true });
    }

    let oldvalue = endnew;
    this.formTrip.controls['endDate'].reset();
    this.formTrip.controls['endDate'].patchValue(oldvalue);
    return this.formTrip.controls['endDate'].setErrors({ 'invaliddaterange': false });
  }

  showCantNumberOfSeats(){
    this.selection.clear();
    let busSelected = this.formTrip.get(["bus"])?.value;
    if (busSelected != null){
      let indexBus = this.buses.findIndex(obj => obj.id === busSelected);
      let bus: Bus = this.buses[indexBus];
      this.cantNumberOfSeats = bus.numberOfSeats - this.selection.selected.length;
    }


  }

}
