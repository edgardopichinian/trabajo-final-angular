import {Component, Inject, OnInit} from '@angular/core';
import {Bus} from "../../domain/bus";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-bus-detail-dialog',
  templateUrl: './bus-detail-dialog.component.html',
  styleUrls: ['./bus-detail-dialog.component.css']
})
export class BusDetailDialogComponent implements OnInit {

  bus: Bus;

  constructor(
    public dialogRef: MatDialogRef<BusDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Bus) {
    this.bus = data;
  }

  onNoClick(): void {
    this.dialogRef.close({event: 'cancel'});
  }

  ngOnInit(): void {
  }

}
