import {Component, OnInit, ViewChild} from '@angular/core';
import {Trip} from "../../domain/trip";
import {TripService} from "../trip.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Bus} from "../../domain/bus";
import {MatDialog} from "@angular/material/dialog";
import {BusDetailDialogComponent} from "../bus-detail-dialog/bus-detail-dialog.component";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'trips-list',
  templateUrl: './trips-list.component.html',
  styleUrls: ['./trips-list.component.css']
})
export class TripsListComponent implements OnInit {

  displayedColumns: string[] = [
    'id', 'departure', 'destination', 'startDate', 'endDate'
    , 'bus.licensePlate', 'bus.brand', 'bus.model'
    , 'option'
  ];
  groupedColumns: string[] = ['trip', 'bus'];
  trips = new MatTableDataSource<Trip>();
  loading: boolean = false;
  grouped: any;

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;

  constructor(public router: Router,
              private tripService: TripService,
              public snackBar: MatSnackBar,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.findAll();
  }

  findAll() {
    this.loading = true;
    this.tripService.findAll().subscribe(list => {
      this.trips = new MatTableDataSource<Trip>(list);
      this.trips.paginator = this.paginator
      this.loading = false;
    });
  }

  goToDetail(t: Trip | null){
    if (t == null)
      this.router.navigate(['trips', 'info']);
    else
      this.router.navigate(['trips', 'info', {id: t.id}]);
  }

  goToBusDetail(bus: Bus | null) {
    const dialogRef = this.dialog.open(BusDetailDialogComponent, {
      width: '25%',
      height: 'auto',
      minHeight: 200,
      data: bus
    });
  }
}
