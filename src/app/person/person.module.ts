import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonRoutingModule } from './person-routing.module';
import { PersonInfoComponent } from './person-info/person-info.component';
import { PersonsListComponent } from './persons-list/persons-list.component';
import {PersonService} from "./person.service";
import {MaterialModule} from "../material/material.module";

@NgModule({
  declarations: [
    PersonInfoComponent,
    PersonsListComponent
  ],
  imports: [
    CommonModule,
    PersonRoutingModule,
    MaterialModule
  ],
  providers: [
    PersonService
  ]
})
export class PersonModule { }
