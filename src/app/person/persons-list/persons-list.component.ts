import {Component, OnInit, ViewChild} from '@angular/core';
import {Person} from "../../domain/person";
import {Router} from "@angular/router";
import {PersonService} from "../../person/person.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DialogData} from "../../../dialog/dialog-data";
import {DeleteConfirmDialogComponent} from "../../../dialog/delete-confirm-dialog/delete-confirm-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.css']
})
export class PersonsListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'lastName', 'age', 'option'];
  persons = new MatTableDataSource<Person>();
  loading: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;

  constructor(public router: Router,
              private personService: PersonService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.findAll();
  }

  findAll() {
    this.loading = true;
    this.personService.findAll().subscribe(list => {
      this.persons = new MatTableDataSource<Person>(list);
      this.persons.paginator = this.paginator
      this.loading = false;
    });
  }

  goToDetail(p: Person | null) {
    if (p == null)
      this.router.navigate(['persons', 'info']);
    else
      this.router.navigate(['persons', 'info', {id: p.id}]);
  }

  delete(id: number) {
    let dialogData = new DialogData(null,
      "¿Está seguro que desea eliminar la persona?", "Confirmación para eliminar");
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: '300px',
      height: 'auto',
      minHeight: 200,
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'delete') {
        this.loading = true;
        this.personService.delete(id).subscribe(p => {
            this.findAll()
            this.snackBar.open("La persona se elimino con exito", 'Éxito', {duration: 2000});
          },
          error => {
            this.snackBar.open(error, "Error", {duration: 2000});
            this.loading = false;
          });
      }
    });
  }
}
